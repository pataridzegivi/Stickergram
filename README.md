# Stickergram
Native iOS application for easy search for telegram stickers from database (with Firebase and Algolia)

1. pod install
2. add GoogleInfo.plist
3. Register account on Algolia and add appID and apiKey to RemoteConfiguration.plist

Enjoy and contribute new features
