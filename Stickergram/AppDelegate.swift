//
//  AppDelegate.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import Fabric
import Crashlytics
import Kingfisher
import KingfisherWebP
import KeychainAccess

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		configureCoreSystems()
//		configureAPN()
		configureRootController()
		
//		FIRStickerSetsDataSource()
		return true
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
		print("applicationWillEnterForeground")
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
		print("applicationDidBecomeActive")
	}

	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
		print(userInfo)
	}
	
	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		print(userInfo)
		completionHandler(.newData)
	}
	
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		print(deviceToken)
	}
	
	func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
		print(notification.debugDescription)
	}
	
	func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
		print(notificationSettings.debugDescription)
	}
	
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print(error.localizedDescription)
	}
}

@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		print(response.debugDescription)
		completionHandler()
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		print(notification.debugDescription)
		completionHandler([.alert,.sound,.badge])
	}
}

extension AppDelegate {
	private func configureRootController() {
		UIView.appearance().tintColor = Colors.base
		window = UIWindow(frame: UIScreen.main.bounds)
		window?.rootViewController = AuthViewController.create()
		window?.makeKeyAndVisible()
	}
	
	private func configureCoreSystems() {
		FirebaseApp.configure()
		Fabric.with([Crashlytics.self])
		Config.configureRemoteConfig()
	}
	
	private func configureAPN() {
		let application = UIApplication.shared
		if #available(iOS 10.0, *) {
			// For iOS 10 display notification (sent via APNS)
			UNUserNotificationCenter.current().delegate = self
			
			let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
			UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (flag, error) in
			}
		} else {
			let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
			application.registerUserNotificationSettings(settings)
		}
		application.registerForRemoteNotifications()
	}
}
