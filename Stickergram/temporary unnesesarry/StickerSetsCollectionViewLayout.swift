//
//  StickerSetsCollectionViewLayout.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

enum StickerSetsLayoutMode {
	case expanded
	case compact
}

protocol StickerSetsCollectionViewLayoutDelegate: class {
	func collectionViewCompactMode() -> Bool
	func collectionViewHeaderHeight() -> CGFloat
	func collectionViewFooterHeight() -> CGFloat
	func collectionViewTagsAreaHeight(forCompactLayoutMode: Bool) -> CGFloat
	func collectionViewStickerCellSize() -> CGSize
}

class StickerSetsCollectionViewLayout: UICollectionViewLayout {

	var stickerSize: CGSize = CGSize.zero
	var headerHeight: CGFloat = 0
	var footerHeight: CGFloat = 0
	
	let stickersSectionIndex: Int = 1
	
	var contentWidth: CGFloat = 0
	weak var delegate: StickerSetsCollectionViewLayoutDelegate!
	
	var cellsAttributesCache: [Int: [UICollectionViewLayoutAttributes]] = [:]
	var headersAttributesCache: [Int: UICollectionViewLayoutAttributes] = [:]
	var numberOfStickerSets: Int = 0
	
	override func prepare() {
		assert(delegate != nil)
		
		stickerSize = delegate.collectionViewStickerCellSize()
		headerHeight = delegate.collectionViewHeaderHeight()
		footerHeight = delegate.collectionViewFooterHeight()

		guard let collectionView = collectionView else { return }
		let numberOfSections = collectionView.numberOfSections
		contentWidth = collectionView.frame.size.width
		
		for section in 0..<numberOfSections {
			let headerAttributes = prepareHeaderAttributes(forSection: section, contentWidth: contentWidth)
			headersAttributesCache[section] = headerAttributes
		}

		cellsAttributesCache[0] = prepareTagsSectionAttributes(contentWidth: contentWidth)
		
		var stickerCellsAttributes: [UICollectionViewLayoutAttributes] = []
		let itemsInStickersSection = collectionView.numberOfItems(inSection: stickersSectionIndex)
		numberOfStickerSets = itemsInStickersSection
		for item in 0..<itemsInStickersSection {
			let indexPath = IndexPath(item: item, section: stickersSectionIndex)
			let attributes = prepareStickerCellAttributes(indexPath: indexPath, contentWidth: contentWidth)
			stickerCellsAttributes.append(attributes)
		}
		cellsAttributesCache[stickersSectionIndex] = stickerCellsAttributes
	}
	
	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
		for (_, attributes) in cellsAttributesCache {
			for attribute in attributes {
				if attribute.frame.intersects(rect) {
					visibleLayoutAttributes.append(attribute)
				}
			}
		}
		for (_, attributes) in headersAttributesCache {
			if attributes.frame.intersects(rect) {
				visibleLayoutAttributes.append(attributes)
			}
		}
		return visibleLayoutAttributes
	}
	
	override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		return cellsAttributesCache[indexPath.section]?[indexPath.item]
	}
	
	override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		guard elementKind == "UICollectionElementKindSectionHeader" else { return nil }
		return headersAttributesCache[indexPath.section]
	}
	
	override var collectionViewContentSize: CGSize {
		let itemsPerRow = Int(contentWidth / stickerSize.width)
		var stickerSectionHeight = headerHeight
		if itemsPerRow != 0 {
			stickerSectionHeight += CGFloat(numberOfStickerSets / itemsPerRow) * stickerSize.height
		}
		let height = heightOfTagSearch() + headerHeight + stickerSectionHeight
		return CGSize(width: contentWidth, height: height)
	}
	
	override func invalidateLayout() {
		super.invalidateLayout()
		cellsAttributesCache = [:]
		headersAttributesCache = [:]
	}
	
	override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
		return true
	}
}

private extension StickerSetsCollectionViewLayout {
	
	func prepareStickerCellAttributes(indexPath: IndexPath, contentWidth: CGFloat) -> UICollectionViewLayoutAttributes {
		let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
		let itemsPerRow = Int(contentWidth / stickerSize.width)
		let indexInRow = indexPath.item % itemsPerRow
		let widthValue = contentWidth / CGFloat(itemsPerRow)
		let xValue = CGFloat(indexInRow) * widthValue
		let rowsBefore = CGFloat(indexPath.item / itemsPerRow)
		let yValue = (headerHeight * 2) + footerHeight + heightOfTagSearch() + (rowsBefore * stickerSize.height)
		attributes.frame = CGRect(x: xValue, y: yValue, width: widthValue, height: stickerSize.height)
		return attributes
	}
	
	func prepareTagsSectionAttributes(contentWidth: CGFloat) -> [UICollectionViewLayoutAttributes] {
		let height = heightOfTagSearch()
		let searchIndexPath = IndexPath(item: 0, section: 0)
		let searchAttributes = UICollectionViewLayoutAttributes(forCellWith: searchIndexPath)
		searchAttributes.frame = CGRect(x: 0, y: headerHeight, width: contentWidth, height: height)
		let showMoreIndexPath = IndexPath(item: 1, section: 0)
		let showMoreAttributs = UICollectionViewLayoutAttributes(forCellWith: showMoreIndexPath)
		showMoreAttributs.frame = CGRect(x: 0, y: headerHeight + height, width: contentWidth, height: footerHeight)
		return [searchAttributes, showMoreAttributs]
	}
	
	func prepareHeaderAttributes(forSection: Int, contentWidth: CGFloat) -> UICollectionViewLayoutAttributes {
		let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader", with: IndexPath(item: 0, section: forSection))
		let yValue = forSection == 0 ? 0 : heightOfTagSearch() + headerHeight + footerHeight
		attributes.frame = CGRect(x: 0, y: yValue, width: contentWidth, height: headerHeight)
		return attributes
	}
	
	func heightOfTagSearch() -> CGFloat {
		let compactMode = delegate.collectionViewCompactMode()
		return delegate.collectionViewTagsAreaHeight(forCompactLayoutMode: compactMode)
	}
}
