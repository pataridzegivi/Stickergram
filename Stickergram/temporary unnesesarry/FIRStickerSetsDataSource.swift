//
//  FIRStickerSetsDataSource.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import ObjectMapper
import FirebaseDatabase

enum StickerOrder {
	case priority
	case name
	case lastAdded
}

class FIRStickerSetsDataSource: NSObject {
	
	private var dataBase: DatabaseReference
	
	override init() {
		self.dataBase = Database.database().reference()
		super.init()
		self.start()
	}
	
	private func start() {
		let refStickers = dataBase.child(Config.Database.stickers)
//		let refTags = dataBase.child(Config.Database.tags)

		
		
//		var newDic = dic
//		for (key, value) in dic {
//			guard var tag = value as? [String: Any] else { continue }
//			tag["tag"] = key
//			newDic[key] = tag
//		}

		///Добавление новой ноды
//		dataBase.child("ru_tags").setValue(dic)

		///Преобразованите тегов в стикере в формат массива c child _tags
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			var tagsNew: [String] = []
//			for (_, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let name = sticker["name"] as? String else { return }
//				guard let tags = sticker["lang"] as? [String: Bool] else { continue }
//				tagsNew = Array(tags.keys)
//				refStickers.child(name).updateChildValues(["_lang": tagsNew])
//			}
//		}
		
		///Удаление child
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			for (_, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let name = sticker["objectID"] as? String else { return }
//				refStickers.child(name).child("name").removeValue()
//			}
//		}
		
	    ///Добавление child
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			var initialRating: Int = 1000
//			for (_, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let name = sticker["name"] as? String else { return }
//				refStickers.child(name).child("objectID").setValue(name)
////				initialRating -= 1
//			}
//		}
		
		///Преобразованите языка стикера в формат массива c child _lang
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			var tagsNew: [String] = []
//			for (_, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let name = sticker["name"] as? String else { return }
//				guard let tags = sticker["lang"] as? [String: Bool] else { continue }
//				tagsNew = Array(tags.keys)
//				refStickers.child(name).updateChildValues(["_lang": tagsNew])
//			}
//		}
		
		///Преобразование тегов в новый формат со статистикой
//		refTags.observeSingleEvent(of: .value) { (snapshot) in
//			let tags = snapshot.value as! [String: Any]
//			for (key, _) in tags {
//				let tagsNew: [String : Any] = ["daily": 0,
//											   "weekly": 0,
//											   "monthly": 0,
//											   "total": 0,
//											   "adult": false,
//											   "rating": 0,
//											   "tag": key]
//				refTags.child(key).setValue(tagsNew)
//			}
//		}
		
		///Добавление ObjectID
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			for (_, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let name = sticker["name"] as? String else { return }
//				refStickers.child(name).child("objectID").setValue(name)
//			}
//		}
	
		///Проставления рейтинга в тегах
		refStickers.observeSingleEvent(of: .value) { (snapshot) in
			let stickers = snapshot.value as! [String: Any]
//			var initialRating = 1000
			for (key, _) in stickers {
				refStickers.child(key).child("published").setValue(false)
//				initialRating -= 1
			}
		}
		
		///Разные запросы
//		let query = refStickers
//			.queryOrdered(byChild: "lang/RU")
//			.queryEqual(toValue: true)
//			.queryLimited(toFirst: count)
//
//		query.observeSingleEvent(of: .value) { (snapshot) in
//			let sets = snapshot.toStickerSets()
//			print(sets.last?.name)
//			guard let lastName = sets.last?.name else { return }
//			let newquery = refStickers
//				.queryOrdered(byChild: "lang/RU")
//				.queryEqual(toValue: true)
//				.queryLimited(toFirst: count)
//
//			newquery.observeSingleEvent(of: .value) { (snapshot) in
//				print(snapshot.value)
//			}
//		}
//
		
		//.queryOrdered(byChild: "tags/картинки").queryEqual(toValue: true)
		
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			for child in snapshot.children {
//				guard let snapshot = child as? DataSnapshot else { continue }
//				guard let set = Mapper<StickerSet>().map(JSONObject: snapshot.value) else { continue }
//				if set.updated == nil {
//					refStickers.child("\(snapshot.key)/added").setValue(ServerValue.timestamp())
//				}
//			}
//		}
		
//		refStickers.queryOrdered(byChild: "_tag").observeSingleEvent(of: .value) { (snapshot) in
//			print(snapshot)
//		}

		
//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			for (key, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let lang = sticker["lang"] as? String else { continue }
//				var newLangs: [String: Bool] = [:]
//
//				if lang == "NA" {
//					newLangs["EN"] = true
//					newLangs["RU"] = true
//				} else if lang == "RU" {
//					newLangs["RU"] = true
//				} else {
//					newLangs[lang] = true
//				}
//				print(newLangs)
//				refStickers.child(key).updateChildValues(["lang": newLangs])
//			}
//		}

//		refStickers.observeSingleEvent(of: .value) { (snapshot) in
//			let stickers = snapshot.value as! [String: Any]
//			var tagsNew = Set<String>.init()
//			for (_, value) in stickers {
//				let sticker = value as! [String: Any]
//				guard let tags = sticker["tags"] as? [String: Bool] else { continue }
//				tagsNew.formUnion(tags.keys)
//			}
//			let filetered = tagsNew.filter({ $0.components(separatedBy: CharacterSet(charactersIn: "\'")).count == 1 })
//
//			var filteredDic: [String: Bool] = [:]
//			for item in filetered {
//				filteredDic[item] = true
//			}
//			print(filteredDic)
//			refTags.child("RU").setValue(filteredDic)
//		}

//		let _ = dataBase.child(FIR.stickers)..observe(.childAdded, andPreviousSiblingKeyWith: { [weak self] (snapshot, key) in
//			guard let sSelf = self else { return }
//			guard let json = snapshot.value as? [String: [String: Any]] else { return }
//			guard let stickerPacksArray = Mapper<StickerSet>().mapDictionary(JSON: json) else { return }
//			sSelf.stickerSets.append(contentsOf: Array(stickerPacksArray.values))
//			sSelf.delegate?.didAddedStickers(setd: Array(stickerPacksArray.values))
//		})
	}
}

