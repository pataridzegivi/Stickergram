//
//  URLs.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 04.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation

struct URLs {
	static func cloudStorageFile(name: String) -> String  {
		let urlWithPercents = name.replacingOccurrences(of: "/", with: "%2F")
		let url = NSString(format: Config.Storage.url as NSString, Config.Storage.bucket, urlWithPercents)
		return url as String
	}
}
