//
//  Notifications.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 18.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation

extension Notification {
	static var clearSelectedTags: Notification {
		return Notification(name: Notification.Name("com.gp-apps.stickergram.clear_selected_tags"))
	}
	
	static var reloadTagsHeader: Notification {
		return Notification(name: Notification.Name("com.gp-apps.stickergram.reload_tags_header"))
	}
}
