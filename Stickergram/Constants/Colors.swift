//
//  Colors.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 04.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import SwiftHEXColors
import UIKit

struct Colors {
	static let base = UIColor(hexString: "#0088CC")!
	static let red = UIColor(hexString: "#FF2D55")!
}
