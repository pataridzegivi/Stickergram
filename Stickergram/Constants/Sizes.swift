//
//  Sizes.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 04.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

struct Sizes {
	struct StickerSetsCollectionView {
		static let cellRatio: CGFloat = 1.15
		static let cellsPerLine: CGFloat = 2
		static let interitemSpacing: CGFloat = 8
		static let lineSpacing: CGFloat = 8
	}
	struct StickersCollectionView {
		static let cellRatio: CGFloat = 1
		static let cellsPerLine: CGFloat = 3
		static let interitemSpacing: CGFloat = 8
		static let lineSpacing: CGFloat = 8
	}
}
