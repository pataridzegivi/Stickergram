//
//  CoreStack.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 12.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import Sync

class CoreStack {
	
	static let shared = CoreStack()
	
	lazy private var dataStack = DataStack(modelName: "Stickers", storeType: .sqLite)
	
	static var stack: DataStack {
		return shared.dataStack
	}
	
	static var mainContext: NSManagedObjectContext {
		return shared.dataStack.mainContext
	}
}

