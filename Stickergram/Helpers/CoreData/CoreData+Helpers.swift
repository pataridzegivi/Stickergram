//
//  Tag+Helpers.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 12.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import CoreData

extension NSManagedObject {
	static func className() -> String {
		return String(describing: self.self)
	}
}
