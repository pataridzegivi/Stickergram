//
//  AuthManager.swift
//  Stickergram
//
//  Created by Givi Pataridze on 26.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import Kingfisher
import FirebaseAuth

struct AuthManager {

	static func authorize(completion: ((Error?)->())?) {
		if let user = Auth.auth().currentUser {
			getToken(user: user, completion: completion)
		} else {
			Auth.auth().signInAnonymously() { (authData, error) in
				guard let user = authData?.user else { return }
				getToken(user: user, completion: completion)
			}
		}
	}
	
	static func getToken(user: User, completion: ((Error?)->())?) {
		user.getIDToken() { (token, error) in
			if let token = token {
				Keychains.setGoogleAccessToken(token: token)
				Kingfisher<Any>.configure(debug: Config.debug)
			}
			completion?(error)
		}
	}
	
	static func refreshToken() {
		guard let user = Auth.auth().currentUser else { return }
		getToken(user: user, completion: nil)
	}
}
