//
//  Keychains.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 04.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import KeychainAccess

struct Keychains {
	static var googleAccessToken: String? {
		let keychain = Keychain(service: "GoogleAuth")
		return keychain["token"]
	}
	
	static func setGoogleAccessToken(token: String) {
		let keychain = Keychain(service: "GoogleAuth")
		keychain["token"] = token
	}
}
