//
//  IndexPath+Helper.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 25.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation

extension IndexPath {
	static func indexes(_ array: [Int], section: Int) -> [IndexPath] {
		return array.map { element -> IndexPath in
			return IndexPath(item: element, section: section)
		}
	}
}
