//
//  UIView+Image.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 11.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

extension UIView {
	func imageCapture() -> UIImage? {
		UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
		if let context = UIGraphicsGetCurrentContext() {
			self.layer.render(in: context)
			let image = UIGraphicsGetImageFromCurrentImageContext()
			UIGraphicsEndImageContext()
			return image
		}
		return nil
	}
}
