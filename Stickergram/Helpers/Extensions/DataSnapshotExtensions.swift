//
//  DataSnapshotExtensions.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 05.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import ObjectMapper
import FirebaseDatabase

extension DataSnapshot {
	func toStickerSet() -> StickerSet? {
		return Mapper<StickerSet>().map(JSONObject: self.value)
	}
	
//	func toStickerSets() -> [StickerSet] {
//		var stickerSets: [StickerSet] = []
//		for child in self.children {
//			guard let snapshot = child as? DataSnapshot else { continue }
//			guard var set = snapshot.toStickerSet() else { continue }
//			set.priority = (snapshot.priority as? Int) ?? 100000
//			stickerSets.append(set)
//		}
//		return stickerSets.sorted(by: { $0.priority < $1.priority })
//	}
}
