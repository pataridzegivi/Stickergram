//
//  Storyboardable.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

protocol Storyboardable {
	static func storyBoardName() -> String
}

extension Storyboardable where Self: UIViewController {
	static func create() -> Self {
		let storyboard = self.storyboard()
		let className = NSStringFromClass(Self.self)
		let finalClassName = className.components(separatedBy: ".").last!
		let viewControllerId = finalClassName + "ID"
		let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerId)
		return viewController as! Self
	}
	
	static func storyboard() -> UIStoryboard {
		return UIStoryboard(name: storyBoardName(), bundle: nil)
	}
}
