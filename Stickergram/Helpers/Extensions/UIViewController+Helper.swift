//
//  Sizes+helper.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 10.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

extension UIViewController {
	func navBarHeight() -> CGFloat {
		var navBar: CGFloat = 0
		if let height = navigationController?.navigationBar.frame.height {
			navBar += height
		}
		return UIApplication.shared.statusBarFrame.height + navBar
	}
	
	func tabBarHeight() -> CGFloat {
		return tabBarController?.tabBar.frame.height ?? 0
	}
	
	func frameWithoutSysContainers() -> CGRect {
		let baseFrame = view.frame
		let newFrame = CGRect(x: baseFrame.minX,
							  y: baseFrame.minY + navBarHeight(),
							  width: baseFrame.width,
							  height: baseFrame.height - tabBarHeight() - navBarHeight())
		return newFrame
	}
}

extension UIViewController {
	static var className: String {
		return String(describing: self.self)
	}
}
