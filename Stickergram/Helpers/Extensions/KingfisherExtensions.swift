//
//  KingfisherExtensions.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 04.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import FirebaseAuth
import Kingfisher
import KingfisherWebP

extension Kingfisher where Base: ImageView {
	@discardableResult
	public func setWebPImage(with resource: Resource?,
							 placeholder: Placeholder? = nil,
							 options: KingfisherOptionsInfo? = nil,
							 progressBlock: DownloadProgressBlock? = nil,
							 completionHandler: CompletionHandler? = nil) -> RetrieveImageTask
	{
		let completion: CompletionHandler = { (image, error, cacheType, url) in
			if let error = error {
				if error.httpCode == 403 {
					AuthManager.refreshToken()
				}
//				DDLogError("\(error.description) for url: \(url?.absoluteString ?? "no url")");
			}
			completionHandler?(image, error, cacheType, url)
		}
		let imageTask = setImage(with: resource, placeholder: placeholder, options: options, progressBlock: progressBlock, completionHandler: completion)
		return imageTask
	}
}

extension Kingfisher where Base: Any {
	static func configure(debug: Bool) {
		if debug {
			let cache = ImageCache.default
			cache.calculateDiskCacheSize { size in
				debugPrint("Used disk size by bytes: \(size)")
			}
			cache.clearMemoryCache()
			cache.clearDiskCache()
		}
	
		var settings: KingfisherOptionsInfo = [.processor(WebPProcessor.default),
											   .cacheSerializer(WebPSerializer.default),
											   .backgroundDecode]
		if Config.Storage.auth, let authUid = Keychains.googleAccessToken {
			let modifier = AnyModifier { request in
				var modifiedRequest = request
				modifiedRequest.setValue("Bearer \(authUid)", forHTTPHeaderField: "Authorization")
				return modifiedRequest
			}
			settings.append(.requestModifier(modifier))
		}
		KingfisherManager.shared.defaultOptions = settings
	}
}
