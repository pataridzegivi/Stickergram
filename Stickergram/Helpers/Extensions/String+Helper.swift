//
//  String+Helper.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 10.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

extension String {
	func widthWithConstrainedHeight(height: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
		let boundingBox = self.boundingRect(with: constraintRect,
											options: NSStringDrawingOptions.usesLineFragmentOrigin,
											attributes: [NSAttributedStringKey.font: font],
											context: nil)
		
		return boundingBox.width
	}
}
