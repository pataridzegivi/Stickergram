//
//  JSONObject+Log.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 08.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import AlgoliaSearch

extension Dictionary where Key == String, Value == Any {
	@discardableResult
	func prettyJson() -> String? {
		guard let data = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted) else { return nil }
		guard let json = String(data: data, encoding: .utf8) else { return nil }
		debugPrint(json)
		return json
	}
}
