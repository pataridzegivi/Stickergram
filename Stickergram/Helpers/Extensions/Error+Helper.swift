//
//  Error+Helper.swift
//  Stickergram
//
//  Created by Givi Pataridze on 26.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation

extension NSError {
	var httpCode: Int {
		if let code = userInfo["statusCode"] as? Int {
			return code
		}
		return -1
	}
}
