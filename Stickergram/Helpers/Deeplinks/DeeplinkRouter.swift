//
//  DeeplinkRouter.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 05.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

struct TelegramRouter {
	static func addSet(set: StickerSetProtocol) {
		guard let name = set.name else { return }
		guard let url = URL(string: "tg://addstickers?set=\(name)") else { return }
		if #available(iOS 10.0, *) {
			UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
				if success {
					Analys.log(.achievement(goal: .addedSticker(to: .telegram, item: .stickerSet, itemId: name)))
				}
			})
		} else {
			UIApplication.shared.openURL(url)
			Analys.log(.achievement(goal: .addedSticker(to: .telegram, item: .stickerSet, itemId: name)))
		}
	}
}
