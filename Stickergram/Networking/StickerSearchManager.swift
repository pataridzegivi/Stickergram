//
//  StickerSearchManager.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 08.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import ObjectMapper
import AlgoliaSearch

enum StickerFinderType {
	case byTag(tags: [String])
	case byQuery(query: String)
	case byRating
}

protocol StickerSearchManagerDelegate: class {
	func didReceived(response: [StickerSet], type: StickerFinderType, firstPage: Bool)
	func didReceived(error: Error,  type: StickerFinderType)
	func didFinished(type: StickerFinderType)
}

class StickerSearchManager {
	weak var delegate: StickerSearchManagerDelegate?
	
	private let client: Client
	private let index: Index

	var currentPage: UInt = 0
	var totalPages: UInt = 0
	
	private let pageCount: UInt
	
	private var searchType: StickerFinderType!
	
	init(delegate: StickerSearchManagerDelegate, pageCount: UInt) {
		self.delegate = delegate
		self.client = Client(appID: Config.Algolia.appID, apiKey: Config.Algolia.apiKey)
		self.index = client.index(withName: Config.Algolia.index)
		self.pageCount = pageCount
	}
	
	func start(type: StickerFinderType) {
		self.searchType = type
		self.currentPage = 0
		let query = queryComposer(type: type, page: currentPage, count: pageCount)
		search(with: query, type: type)
		currentPage += 1
	}
	
	func next() {
		let query = queryComposer(type: searchType, page: currentPage, count: pageCount)
		if totalPages > currentPage {
			search(with: query, type: searchType)
			currentPage += 1
		} else {
			delegate?.didFinished(type: searchType)
		}
	}
	
	private func errorComposer(domain: String) -> Error {
		let error = NSError(domain: domain, code: -1, userInfo: nil)
		return error
	}
		
	private func queryComposer(type: StickerFinderType, page: UInt, count: UInt) -> Query {
		switch type {
		case .byQuery(query: let query):
			let query = Query(query: query)
			query.page = page
			query.hitsPerPage = count
			return query
		case .byTag(tags: let tags):
			let query = Query(parameters: [:])
			query.tagFilters = [tags]
			query.hitsPerPage = count
			return query
		case .byRating:
			let query = Query(parameters: [:])
			query.page = page
			query.hitsPerPage = count
			return query
		}
	}
	
	private func search(with query: Query, type: StickerFinderType) {
		index.search(query, completionHandler: { [weak self] (response, error) in
			guard let sSelf = self else { return }
			if let error = error {
				sSelf.delegate?.didReceived(error: error, type: type)
			} else {
				guard let response = response else { return }
				guard let searchResult = Mapper<AlgoliaResponse>().map(JSON: response) else { return }
				sSelf.totalPages = searchResult.nbPages
				guard let stickerSets = searchResult.hits else { return }
				sSelf.delegate?.didReceived(response: stickerSets, type: type, firstPage: (query.page == 0) || (query.page == nil))
			}
		})
	}
}
