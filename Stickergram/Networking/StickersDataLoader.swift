//
//  StickersDataLoader.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 26.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import AlgoliaSearch


protocol StickersDataLoaderDelegate: class {
	func add(stickers: [StickerSet], firstPage: Bool)
}

class StickersDataLoader: NSObject {

	private weak var delegate: StickersDataLoaderDelegate?
	private var networkManager: StickerSearchManager!
	
	init(delegate: StickersDataLoaderDelegate, pageCount: UInt) {
		self.delegate = delegate
		super.init()
		self.networkManager = StickerSearchManager(delegate: self, pageCount: pageCount)
	}
	
	func next() {
		networkManager.next()
	}
	
	func getStickersByRating() {
		networkManager.start(type: .byRating)
	}
	
	func getStickersBy(tags: [String]) {
		networkManager.start(type: .byTag(tags: tags))
	}
	
	func getStickersBy(query: String) {
		networkManager.start(type: .byQuery(query: query))
	}	
}

extension StickersDataLoader: StickerSearchManagerDelegate {
	func didReceived(response: [StickerSet], type: StickerFinderType, firstPage: Bool) {
		switch type {
		case .byQuery(query: _):
			delegate?.add(stickers: response, firstPage: firstPage)
		case .byTag(tags: _):
			delegate?.add(stickers: response, firstPage: firstPage)
		case .byRating:
			delegate?.add(stickers: response, firstPage: firstPage)
		}
	}
	
	func didReceived(error: Error, type: StickerFinderType) {
	}
	
	func didFinished(type: StickerFinderType) {
	}
}
