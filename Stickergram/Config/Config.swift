//
//  Config.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 03.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import Firebase

struct Config {
	static let debug = true
	static let defaultConfig = RemoteConfig.remoteConfig()
	static let configExpiration: TimeInterval = 120
	static let defaultLocale = "ru"
	
	static var locale: String {
		return defaultLocale
//		let str = Locale.preferredLanguages.first!
//		let characterSet = CharacterSet(charactersIn: "-")
//		guard let lang = str.components(separatedBy: characterSet).first else { return defaultLocale }
//		return lang
	}
	static var isTagsEnabled = false
	
	struct Storage {
		static var url: String {
			return defaultConfig["storage_url"].stringValue!
		}
		
		static var auth: Bool {
			return defaultConfig["storage_auth"].boolValue
		}
		
		static var bucket: String {
			return defaultConfig["storage_bucket"].stringValue!
		}
	}
	
	struct Algolia {
		static var appID: String {
			return defaultConfig["algolia_appID"].stringValue!
		}
		
		static var apiKey: String {
			return defaultConfig["algolia_apiKey"].stringValue!
		}
		
		static var index: String {
			let indexKey = defaultConfig["algolia_index"].stringValue!
			return NSString(format: indexKey as NSString, Config.locale) as String
		}
		
		static var itemsPerRequest: Int {
			return 20
		}
	}
	
	struct Database {
		static var tags: String {
			let tagsKey = defaultConfig["database_tags"].stringValue!
			return NSString(format: tagsKey as NSString, Config.locale) as String
		}
		
		static var stickers: String {
			let stickersKey =  defaultConfig["database_stickers"].stringValue!
			return NSString(format: stickersKey as NSString, Config.locale) as String
		}
		
		static var itemsPerRequest: Int {
			return 20
		}
	}	
	
	static func configureRemoteConfig() {
		let config = RemoteConfig.remoteConfig()
		let settings = RemoteConfigSettings(developerModeEnabled: Config.debug)
		config.configSettings = settings
		config.setDefaults(fromPlist: "RemoteConfiguration")
		config.fetch(withExpirationDuration: configExpiration) { (status, error) in
			config.activateFetched()
		}
	}
}

