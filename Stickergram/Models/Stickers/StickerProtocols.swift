//
//  Sticker.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import AXPhotoViewer
import ObjectMapper

protocol StickerSetProtocol: Mappable {
	var name: String! {get set}
	var title: String! {get set}
	var masks: Bool! {get set}
	var count: Int! {get set}
}

extension StickerSetProtocol {
	func thumbUrl() -> URL? {
		return stickerThumbUrl(number: 0)
	}
	
	func stickerThumbUrl(number: Int) -> URL? {
		guard let stickerName = self.name else { return nil }
		let fileName = "\(stickerName)/\(stickerName)_\(number)_thumb.jpg"
		let url = URL(string: "\(URLs.cloudStorageFile(name: fileName))")
		return url
	}
	
	func stickerImageUrl(number: Int) -> URL? {
		guard let stickerName = self.name else { return nil }
		let fileName = "\(stickerName)/\(stickerName)_\(number).webp"
		let url = URL(string: "\(URLs.cloudStorageFile(name: fileName))")
		return url
	}
	
}

extension StickerSetProtocol {
	func photosDataSource() -> [AXPhotoProtocol] {
		var images: [StickerImage] = []
		for i in 0..<count {
			images.append(photosDataSourceThumb(item: i))
		}
		return images
	}
	func photosDataSourceThumb(item: Int) -> StickerImage {
		let url = stickerImageUrl(number: item)
		let photo = StickerImage()
		photo.url = url
		return photo
	}
}
