//
//  Sticker.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import ObjectMapper
import AXPhotoViewer

struct Sticker: Mappable {
	var fileId: String!
	var width: Int!
	var height: Int!
	var thumb: PhotoSize?
	var emoji: String?
	var setName: String?
	var maskPosition: MaskPosition?
	var fileSize: Int?
	
	init?(map: Map) {
	}
	
	mutating func mapping(map: Map) {
		fileId 	     <- map["file_id"]
		width    	 <- map["width"]
		height 	     <- map["height"]
		thumb    	 <- map["thumb"]
		emoji 	     <- map["emoji"]
		setName      <- map["set_name"]
		maskPosition <- map["mask_position"]
		fileSize     <- map["file_size"]
	}
}


struct PhotoSize: Mappable {
	var fileId: String!
	var width: Int!
	var height: Int!
	var fileSize: Int?
	
	init?(map: Map) {
	}
	
	mutating func mapping(map: Map) {
		fileId 	     <- map["file_id"]
		width    	 <- map["width"]
		height 	     <- map["height"]
		fileSize     <- map["file_size"]
	}
}

struct MaskPosition: Mappable  {
	var point: String!
	var xShift: Float!
	var yShift: Float!
	var scale: Float!
	
	init?(map: Map) {
	}
	
	mutating func mapping(map: Map) {
		point 	     <- map["point"]
		xShift    	 <- map["x_shift"]
		yShift 	     <- map["y_shift"]
		scale    	 <- map["scale"]
	}
}

struct File: Mappable {
	var fileId: String!
	var fileSize: Int?
	var filePath: String?
	
	init?(map: Map) {
	}
	
	mutating func mapping(map: Map) {
		fileId 	     <- map["file_id"]
		fileSize     <- map["file_size"]
		filePath 	 <- map["file_path"]
	}
}

class StickerImage: NSObject, AXPhotoProtocol {
	var imageData: Data?
	var image: UIImage?
	var url: URL?
}
