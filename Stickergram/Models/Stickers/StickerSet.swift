//
//  StickerPack.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import ObjectMapper
import FirebaseDatabase

struct StickerSet: StickerSetProtocol {
	var name: String!
	var title: String!
	var masks: Bool!
	var stickers: [Sticker]?
	var count: Int!
	
	var rating: Int = 0
	var added: Date?
	var updated: Date?
	
	var tags: [String]?
	var lang: [String]?
	
	let transformArrays = TransformOf<[String], [String: Bool]>(fromJSON: { (value: [String: Bool]?) -> [String]? in
		return value?.map { $0.key }
	}, toJSON: { (value: [String]?) -> [String: Bool]? in
		guard let value = value else { return nil }
		var jsonDic: [String: Bool] = [:]
		for item in value {
			jsonDic[item] = true
		}
		return jsonDic
	})
	
	let transformDate = TransformOf<Date, TimeInterval>(fromJSON: { (value: TimeInterval?) -> Date? in
		guard let interval = value else { return nil }
		return Date(timeIntervalSince1970: interval / 1000)
	}, toJSON: { (value: Date?) -> TimeInterval? in
		guard let date = value else { return nil }
		return date.timeIntervalSince1970 * 1000
	})
	
}

extension StickerSet {
	init?(map: Map) {
	}
	
	mutating func mapping(map: Map) {
		name 	     <- map["objectID"]
		title    	 <- map["title"]
		masks 	     <- map["contains_masks"]
		stickers     <- map["stickers"]
		count        <- map["count"]
		rating       <- map["rating"]
		
		tags         <- map["_tags"]
		lang         <- map["_lang"]
		
		added        <- (map["added"], transformDate)
		updated      <- (map["updated"], transformDate)
	}
}
