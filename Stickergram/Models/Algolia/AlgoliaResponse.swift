//
//  AlgoliaResponse.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 08.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import ObjectMapper

struct AlgoliaResponse: Mappable {
	var hitPerPage: String!
	var nbHits: UInt!
	var page: UInt!
	var params: String!
	var processingTime: UInt!
	var query: String!
	var nbPages: UInt!
	
	var hits: [StickerSet]?
	
	init?(map: Map) {
	}
	
	mutating func mapping(map: Map) {
		hitPerPage 	     <- map["hitsPerPage"]
		nbHits    	     <- map["nbHits"]
		hits 	         <- map["hits"]
		page    	     <- map["page"]
		params 	         <- map["params"]
		processingTime   <- map["processingTimeMS"]
		query            <- map["query"]
		nbPages          <- map["nbPages"]
	}
}

