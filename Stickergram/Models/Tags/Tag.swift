//
//  Tag+Extensions.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 19.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import CoreData

public class Tag: NSManagedObject {

	@nonobjc public class func fetchRequest() -> NSFetchRequest<Tag> {
		return NSFetchRequest<Tag>(entityName: "Tag")
	}

	@NSManaged public var added: Date?
	@NSManaged public var adult: Bool
	@NSManaged public var name: String!
	@NSManaged public var rating: Int64
	@NSManaged public var selected: Bool
	@NSManaged public var trending: Bool
	@NSManaged public var type: Int64
	
	public override func willSave() {
		super.willSave()
		if type != computedType() {
			type = computedType()
		}
	}
	
	private func computedType() -> Int64 {
		if selected {
			return 0
		}
		if trending {
			return 1
		}
		return 2
	}
}

