//
//  Analytics.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 02.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import Umbrella
import AXPhotoViewer
import FirebaseAnalytics

struct Analys {
	static let shared = { () -> Umbrella.Analytics<Event> in
		let analytics = Umbrella.Analytics<Event>()
		analytics.register(provider: FirebaseProvider())
		return analytics
	}()
	
	private init() {
		Analytics.setScreenName(Event.Screen.stickersFeed.rawValue, screenClass: StickersFeedViewController.className)
		Analytics.setScreenName(Event.Screen.allTagsList.rawValue, screenClass: TagSearchViewController.className)
		Analytics.setScreenName(Event.Screen.stickersFinder.rawValue, screenClass: StickersSearchViewController.className)
		Analytics.setScreenName(Event.Screen.stickerSetDetail.rawValue, screenClass: StickersViewController.className)
		Analytics.setScreenName(Event.Screen.stickerSetPager.rawValue, screenClass: AXPhotosViewController.className)
	}
	
	static func log(_ event: Event) {
		Analys.shared.log(event)
	}
}

enum Event {
	case open(screen: Screen)
	case tapButton(button: Content.Button)
	case tapItem(item: Content.Item, itemId: String)
	case scroll(list: Content.List, page: UInt)
	case select(item: Content.Item, itemId: String)
	case deselect(item: Content.Item, itemId: String)
	case search(item: Content.Item, query: String)
	case preview(item: Content.Item, itemId: String)
	case achievement(goal: Goal)
	
	enum Target: String {
		case telegram = "telegram"
	}
	
	enum Content {
		enum Button: String {
			case searchStickers = "button_search_stickers"
			case showMoreTags = "button_show_more_tags"
			case proposeSticker = "button_propose_sticker"
			case addSticker = "button_add_sticker"
			case clearAllTags = "button_clear_all_tags"
		}
		enum Item: String {
			case stickerSet = "sticker_set"
			case stickerTag = "sticker_tag"
		}
		enum List: String {
			case popular = "list_popular"
			case trends = "list_trends"
		}
	}
	
	enum Screen: String {
		case stickersFeed     = "screen_stickers_feed"
		case allTagsList      = "screen_full_tags_list"
		case stickersFinder   = "screen_stickers_finder"
		case stickerSetDetail = "screen_sticker_set_detail"
		case stickerSetPager  = "screen_sticker_set_pager"
	}
	
	enum Goal {
		case addedSticker(to: Target, item: Content.Item, itemId: String)
		case proposedSticker(from: Target, item: Content.Item, itemId: String)
	}
}

extension Event: EventType {
	func name(for provider: ProviderType) -> String? {
		switch self {
		case .open(let screen): return "open_\(screen.rawValue)"
		case .tapButton(let button): return "tap_\(button.rawValue)"
		case .tapItem(let item, _): return "tap_\(item)"
		case .scroll(let list, _): return "scroll_\(list)"
		case .select(let item, _): return "select_\(item)"
		case .deselect(let item, _): return "deselect_\(item)"
		case .search(let item, _): return "search_\(item)"
		case .preview(let item, _): return "preview_\(item)"
		case .achievement(let goal):
			switch goal {
			case .addedSticker: return "achievement_added_sticker"
			case .proposedSticker: return "achievement_proposed_sticker"
			}
		}
	}
	
	func parameters(for provider: ProviderType) -> [String: Any]? {
		switch self {
		case .open(let screen):
			return ["screen_id": screen.rawValue]
		case .tapButton(let button):
			return ["button_id": button.rawValue]
		case .tapItem(let item, let itemId):
			return ["item_type": item.rawValue,
					"item_id": itemId]
		case .scroll(let list, let page):
			return ["list_type": list.rawValue,
					"page_id": page]
		case .select(let item, let itemId):
			return ["item_type": item.rawValue,
					"item_id": itemId]
		case .deselect(let item, let itemId):
			return ["item_type": item.rawValue,
					"item_id": itemId]
		case .search(let item, let query):
			return ["item_type": item.rawValue,
					"query": query]
		case .preview(let item, let itemId):
			return ["item_type": item.rawValue,
					"item_id": itemId]
		case .achievement(let goal):
			switch goal {
			case .addedSticker(to: let target, item: let item, itemId: let id):
				return ["target_id": target.rawValue,
						"item_type": item.rawValue,
						"item_id": id]
			case .proposedSticker(from: let target, item: let item, itemId: let id):
				return ["target_id": target.rawValue,
						"item_type": item.rawValue,
						"item_id": id]
			}
		}
	}
}
