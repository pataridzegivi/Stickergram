//
//  InternalAnalyticsProvider.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 02.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Umbrella

struct InternalAnalyticsProvider {
	static let shared = InternalAnalyticsProvider()
	
	func sendStatistics() {
		
	}
}

extension InternalAnalyticsProvider: ProviderType {
	func log(_ eventName: String, parameters: [String: Any]?) {
//		AwesomeAnalytics.logEvent(withName: eventName, parameters: parameters)
	}
}
