//
//  TagSearchDataSource.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 09.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import CoreData
import Sync
import FirebaseDatabase

protocol TagsDataSourceDelegate: class {
	var collectionView: UICollectionView! { get }
	func didLoadingCompleted()
}

class TagSearchDataSource: NSObject {
	
	private let notificationCenter = NotificationCenter.default
	private let dataBase: DatabaseReference
	private let dataSourceType: TagsDataSourceType
	private let tagsCount: Int
	private let sectionsCount: Int
	
	private var fetchResultsController: NSFetchedResultsController<Tag>!
	private weak var container: TagsDataSourceDelegate!
	
	init(container: TagsDataSourceDelegate, type: TagsDataSourceType, tagsCount: Int) {
		self.dataSourceType = type
		self.dataBase = Database.database().reference()
		self.container = container
		self.tagsCount = tagsCount
		if case .multipleSection = type {
			self.sectionsCount = 3
		} else {
			self.sectionsCount = 1
		}
		super.init()
		configureFetchedResultsController(type)
		configureObservers()
		loadData()
	}
	
	func loadData() {
		if let cachedTags = fetchResultsController.fetchedObjects, cachedTags.count < tagsCount {
			fetchNewTagsFromServer()
		} else {
			container.didLoadingCompleted()
		}
	}
	
	func clearSelectedTags() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: Tag.className())
		request.returnsObjectsAsFaults = false
		do {
			let result = try CoreStack.mainContext.fetch(request)
			guard let tags = result as? [Tag] else { return }
			for tag in tags {
				tag.selected = false
			}
			try CoreStack.mainContext.save()
			loadData()
		} catch {
			debugPrint(error.localizedDescription)
		}
	}
	
	deinit {
		notificationCenter.removeObserver(self)
	}
}

private extension TagSearchDataSource {
	func performFetch() {
		do {
			try fetchResultsController.performFetch()
		} catch {
			debugPrint(error)
		}
	}
	
	func configureObservers() {
		notificationCenter.addObserver(forName: Notification.clearSelectedTags.name, object: nil, queue: nil) { [weak self] _ in
			self?.clearSelectedTags()
		}
	}
	
	func configureFetchedResultsController(_ type: TagsDataSourceType) {
		let request = NSFetchRequest<Tag>(entityName: Tag.className())
		var sectionNameKeyPath: String? = nil
		var sortDescriptors: [NSSortDescriptor] = []
		let sortRating = NSSortDescriptor(key: "rating", ascending: false)
		sortDescriptors.append(sortRating)
		switch type {
		case .multipleSection(let key):
			sectionNameKeyPath = key
			let sortSection = NSSortDescriptor(key: key, ascending: true)
			sortDescriptors.insert(sortSection, at: 0)
		case .oneSection:
			let sortSelected = NSSortDescriptor(key: "selected", ascending: false)
			sortDescriptors.append(sortSelected)
		}
		request.sortDescriptors = sortDescriptors
		request.returnsObjectsAsFaults = false
		self.fetchResultsController = NSFetchedResultsController(fetchRequest: request,
																 managedObjectContext: CoreStack.mainContext,
																 sectionNameKeyPath: sectionNameKeyPath,
																 cacheName: nil)
		if case .multipleSection = type {
			self.fetchResultsController.delegate = self
		}
		performFetch()
	}
	
	func fetchNewTagsFromServer() {
		let refTags = dataBase.child(Config.Database.tags)
		let query = refTags.queryLimited(toFirst: UInt(tagsCount))
		query.observeSingleEvent(of: .value) { [weak self] (snapshot) in
			guard var values = snapshot.value as? [String: [String : Any]] else { return }
			CoreStack.stack.sync(Array(values.values), inEntityNamed: Tag.className()) { [weak self] error in
				if let error = error {
					debugPrint(error.localizedDescription)
				} else {
					do {
						try CoreStack.stack.mainContext.save()
						self?.performFetch()
						self?.container.didLoadingCompleted()
					} catch {
						debugPrint(error)
					}
				}
			}
		}
	}
}

extension TagSearchDataSource: NSFetchedResultsControllerDelegate {
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		debugPrint(type.hashValue)
		debugPrint(indexPath ?? "")
		debugPrint(newIndexPath ?? "")
		debugPrint(anObject)
	}

	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
		switch sectionName {
		case "0":
			return NSLocalizedString("Selected", comment: "Selected tags list section title")
		case "1":
			return NSLocalizedString("Trending", comment: "Trending tags list section title")
		case "2":
			return NSLocalizedString("Popular", comment: "Popular tags list section title")
		default:
			return NSLocalizedString("Other", comment: "Other tags list section title")
		}
	}
}

extension TagSearchDataSource: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let tag = fetchResultsController.object(at: indexPath)
		tag.selected = !tag.selected
		if tag.selected {
			Analys.log(.select(item: .stickerTag, itemId: tag.name))
		} else {
			Analys.log(.deselect(item: .stickerTag, itemId: tag.name))
		}
		collectionView.reloadItems(at: [indexPath])
	}
}

extension TagSearchDataSource: TagSearchCollectionViewLayoutDelegate {
	func collectionView(collectionView: UICollectionView, textWidthForItemAtIndexPath indexPath: IndexPath) -> CGFloat {
		guard let tagName = fetchResultsController.object(at: indexPath).name else { return 0 }
		let width = tagName.widthWithConstrainedHeight(height: TagSearchCollectionViewLayout.itemHeight, font: TagCollectionViewCell.tagFont)
		return width
	}
}

extension TagSearchDataSource: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.tagCollectionViewCell, for: indexPath)!
		let tag = fetchResultsController.object(at: indexPath)
		cell.searchTag = tag
		cell.setSelected(selected: tag.selected)
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: R.reuseIdentifier.tagSearchHeaderView, for: indexPath)!
		header.titleLabel.text = fetchResultsController.sections?[indexPath.section].indexTitle
		return header
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		guard let sections = fetchResultsController.sections else { return 0 }
		return sections[section].numberOfObjects
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		guard let sections = fetchResultsController.sections else { return 0 }
		return sections.count
	}
}
