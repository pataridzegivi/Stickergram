//
//  TagSearchHeaderView.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 19.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class TagSearchHeaderView: UICollectionReusableView {

	@IBOutlet weak var titleLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
