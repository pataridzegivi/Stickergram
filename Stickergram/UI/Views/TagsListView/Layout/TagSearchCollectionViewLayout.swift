//
//  TagSearchCollectionViewLayout.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 10.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

protocol TagSearchCollectionViewLayoutDelegate: class {
	func collectionView(collectionView: UICollectionView, textWidthForItemAtIndexPath indexPath: IndexPath) -> CGFloat
}

class TagSearchCollectionViewLayout: UICollectionViewLayout {
	
	static let itemHeight: CGFloat = 38
	
	private var headerHeight: CGFloat = 40
	private let lineMinOffset: CGFloat = 0
	private let contentSideInset: CGFloat = 4
	private let itemInset: CGFloat = 10
	private let interRow: CGFloat = 0
	private let interSectionOffset: CGFloat = 20
	private var rowsPerSection: [Int] = []
	
	private var contentSize: CGSize = CGSize.zero
	weak var delegate: TagSearchCollectionViewLayoutDelegate!
	
	private var attributesCache: [Int: [UICollectionViewLayoutAttributes]] = [:]
	private var headersAttributesCache: [Int: UICollectionViewLayoutAttributes] = [:]
	
	convenience init(type: TagsDataSourceType) {
		self.init()
		if case .oneSection = type {
			headerHeight = 0
		}
	}
	
	override func prepare() {
		guard let collectionView = collectionView, attributesCache.isEmpty else { return }
		let numberOfSections = collectionView.numberOfSections
		let contentWidth = collectionView.frame.size.width - contentSideInset * 2
		
		rowsPerSection = Array<Int>(repeating: 0, count: numberOfSections)
		
		for section in 0..<numberOfSections {
			let headerAttributes = prepareHeaderAttributes(forSection: section, contentWidth: contentWidth)
			headersAttributesCache[section] = headerAttributes
			let attributes = prepareAttributesForSection(collectionView: collectionView, section: section, contentWidth: contentWidth)
			attributesCache[section] = attributes
		}

		let rowsTotalCount = rowsPerSection.reduce(0, + )
		let height = CGFloat(rowsTotalCount) * (TagSearchCollectionViewLayout.itemHeight + lineMinOffset) + lineMinOffset + interSectionOffset + CGFloat(numberOfSections) * headerHeight
		
		contentSize = CGSize(width: contentWidth, height: height)
	}
	
	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
		for (_, attributes) in attributesCache {
			for attribute in attributes {
				if attribute.frame.intersects(rect) {
					visibleLayoutAttributes.append(attribute)
				}
			}
		}
		for (_, attributes) in headersAttributesCache {
			if attributes.frame.intersects(rect) {
				visibleLayoutAttributes.append(attributes)
			}
		}
		return visibleLayoutAttributes
	}
	
	override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		return attributesCache[indexPath.section]?[indexPath.item]
	}
	
	override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		guard elementKind == "UICollectionElementKindSectionHeader" else { return nil }
		return headersAttributesCache[indexPath.section]
	}
	
	override var collectionViewContentSize: CGSize {
		return contentSize
	}
	
	override func invalidateLayout() {
		super.invalidateLayout()
		attributesCache = [:]
		headersAttributesCache = [:]
	}
	
	override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
		return true
	}
}

private extension TagSearchCollectionViewLayout {
	
	func prepareAttributesForSection(collectionView: UICollectionView, section: Int, contentWidth: CGFloat) -> [UICollectionViewLayoutAttributes] {
		let itemsInSection = collectionView.numberOfItems(inSection: section)
		
		var textItemsWidths: [CGFloat] = []
		for item in 0..<itemsInSection {
			let indexPath = IndexPath(item: item, section: section)
			let textWidth = delegate.collectionView(collectionView: collectionView, textWidthForItemAtIndexPath: indexPath)
			textItemsWidths.append(itemInset + textWidth + itemInset)
		}
		
		let cellsSorted = cellsPerRow(with: textItemsWidths, forSection: section, contentWidth: contentWidth)
		rowsPerSection[section] = cellsSorted.count
		return prepareAttributes(cells: cellsSorted, forSection: section, contentWidth: contentWidth)
	}
	
	func cellsPerRow(with items: [CGFloat], forSection: Int, contentWidth: CGFloat) -> [[(CGFloat, IndexPath)]] {
		guard items.count > 0 else { return [] }
		
		var accumWidth: CGFloat = 0
		var widhtTempContainer: [(CGFloat, IndexPath)] = []
		var textWidthsPerRow: [[(CGFloat, IndexPath)]] = []
		
		var index = 0
		while index < items.count {
			let textWith = items[index]
			accumWidth += textWith
			let cellInfo = (textWith, IndexPath(item: index, section: forSection))
			if textWith >= contentWidth {
				textWidthsPerRow.append([(contentWidth, IndexPath(item: index, section: forSection))])
				widhtTempContainer = []
				accumWidth = 0
			} else {
				widhtTempContainer.append(cellInfo)
				if accumWidth > contentWidth {
					widhtTempContainer.removeLast()
					textWidthsPerRow.append(widhtTempContainer)
					widhtTempContainer = [cellInfo]
					accumWidth = textWith
				}
			}
			index += 1
		}
		textWidthsPerRow.append(widhtTempContainer)
		return textWidthsPerRow
	}
	
	func prepareAttributes(cells: [[(CGFloat, IndexPath)]], forSection: Int, contentWidth: CGFloat) -> [UICollectionViewLayoutAttributes] {
		var attributes: [UICollectionViewLayoutAttributes] = []
		var yValue: CGFloat = lineMinOffset + heightAboveSection(section: forSection) + headerHeight
		
		for row in cells {
			let spacesLength = (CGFloat(row.count) - 1) * lineMinOffset
			let baseValue = (contentWidth - spacesLength) / row.reduce(0, { $0 + $1.0 })
			var xValue: CGFloat = contentSideInset
			
			for item in row {
				let attribute = UICollectionViewLayoutAttributes(forCellWith: item.1)
				let widthValue = baseValue * item.0
				let cellFrame = CGRect(x: xValue,
									   y: yValue,
									   width: widthValue,
									   height: TagSearchCollectionViewLayout.itemHeight)
				xValue += widthValue
				attribute.frame = cellFrame
				attributes.append(attribute)
			}
			
			yValue += TagSearchCollectionViewLayout.itemHeight + lineMinOffset
		}
		return attributes
	}
	
	func prepareHeaderAttributes(forSection: Int, contentWidth: CGFloat) -> UICollectionViewLayoutAttributes {
		let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader", with: IndexPath(item: 0, section: forSection))
		attributes.frame = CGRect(x: 0, y: heightAboveSection(section: forSection), width: contentWidth, height: headerHeight)
		return attributes
	}
	
	func sectionHeight(section: Int) -> CGFloat {
		return CGFloat(rowsPerSection[section]) * TagSearchCollectionViewLayout.itemHeight + headerHeight
	}
	
	func heightAboveSection(section: Int) -> CGFloat {
		guard section != 0 else { return 0 }
		var accumHeight: CGFloat = 0
		for item in 0...section - 1 {
			accumHeight += sectionHeight(section: item)
		}
		return accumHeight
	}
}
