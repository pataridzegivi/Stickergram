//
//  TagCollectionViewCell.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 09.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {

	var searchTag: Tag! {
		didSet {
			tagImage.setTitle(searchTag.name, for: .normal)
		}
	}
	
	@IBOutlet private weak var tagImage: UIButton!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        tagImage.layer.cornerRadius = 13
		tagImage.layer.borderColor = Colors.base.cgColor
		tagImage.layer.borderWidth = 1
    }
	
	func setSelected(selected: Bool) {
		if selected {
			tagImage.backgroundColor = Colors.base
			tagImage.setTitleColor(.white, for: .normal)
		} else {
			tagImage.backgroundColor = .clear
			tagImage.setTitleColor(Colors.base, for: .normal)
		}
	}
	
	static var tagFont: UIFont {
		return UIFont.systemFont(ofSize: 18)
	}
}
