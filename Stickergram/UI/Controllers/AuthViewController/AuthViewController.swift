//
//  AuthViewController.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 25.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import Kingfisher
import FirebaseAuth

class AuthViewController: UIViewController {

	@IBOutlet weak var indicator: UIActivityIndicatorView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		AuthManager.authorize { [weak self] (error) in
			if let error = error?.localizedDescription {
				self?.showError(text: error, completion: nil)
			} else {
				let vc = StickersFeedViewController.create()
				let nc = UINavigationController(rootViewController: vc)
				self?.present(nc, animated: true, completion: nil)
			}
		}
	}
}

extension AuthViewController: Storyboardable {
	static func storyBoardName() -> String {
		return "Main"
	}
}
