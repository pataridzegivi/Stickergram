//
//  TagSearchDismissionAnimator.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 10.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class TagSearchDismissionAnimator : NSObject, UIViewControllerAnimatedTransitioning {
	
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return 0.4
	}
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		guard let fromViewController = transitionContext.viewController(forKey: .from),
			let toViewController = transitionContext.viewController(forKey: .to) else { return }
		
		let animationDuration = transitionDuration(using: transitionContext)
		transitionContext.containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)

		UIView.animate(withDuration: animationDuration, animations: {
			fromViewController.view.alpha = 0
		}) { finished in
			transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
		}
	}
}
