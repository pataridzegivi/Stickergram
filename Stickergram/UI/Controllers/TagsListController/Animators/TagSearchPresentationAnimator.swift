//
//  TagSearchPresentationAnimator.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 10.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class TagSearchPresentationAnimator: NSObject {
	var presentedFrame: CGRect!
}

extension TagSearchPresentationAnimator: UIViewControllerAnimatedTransitioning {
	func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
		return 0.4
	}
	
	func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
		guard let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? TagSearchViewController  else { return }
		let containerView = transitionContext.containerView
		let animationDuration = transitionDuration(using: transitionContext)
		
		toViewController.backgroundView = createBlurredBackgroundView(using: transitionContext)

		toViewController.view.clipsToBounds = true
		toViewController.view.alpha = 0
		
		containerView.addSubview(toViewController.view)
		
		UIView.animate(withDuration: animationDuration, animations: {
			toViewController.view.alpha = 1
		}, completion: { finished in
			transitionContext.completeTransition(finished)
		})
	}
}

extension TagSearchPresentationAnimator {
	func createBlurredBackgroundView(using transitionContext: UIViewControllerContextTransitioning) -> UIView? {
		guard !UIAccessibilityIsReduceTransparencyEnabled(),
			let fromViewController = transitionContext.viewController(forKey: .from) else { return nil }
		
		let blurView = UIView()
		blurView.backgroundColor = .clear
		blurView.frame = fromViewController.view.bounds
		blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		blurView.clipsToBounds = true
		
		let imageView = UIImageView(image: fromViewController.view.imageCapture())
		imageView.backgroundColor = .white
		imageView.frame = blurView.bounds
		imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		imageView.clipsToBounds = true
		blurView.addSubview(imageView)

		let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
		blurEffectView.frame = blurView.bounds
		blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		blurView.addSubview(blurEffectView)
		return blurView
	}
}
