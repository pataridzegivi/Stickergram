//
//  TagSearchViewController.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 09.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import Crashlytics

class TagSearchViewController: UIViewController, TagsDataSourceDelegate {
	
	let clearButtonText = NSLocalizedString("Clear", comment: "Button that clears selections for all tags in database")
	let sectionTitle = NSLocalizedString("All tags", comment: "Tags search view controller title")
	
	var dataSource: TagSearchDataSource?
	var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	var backgroundView: UIView! {
		didSet {
			activityIndicator.hidesWhenStopped = true
			activityIndicator.center = backgroundView.center
			backgroundView.addSubview(activityIndicator)
			collectionView.backgroundView = backgroundView
		}
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		automaticallyAdjustsScrollViewInsets = true
		activityIndicator.startAnimating()
		configureNavigationBar()
		configureCollectionView()
	}
	
	func didLoadingCompleted() {
		activityIndicator.stopAnimating()
		collectionView.reloadData()
	}
	
	@objc func removeTags() {
		Analys.log(.tapButton(button: .clearAllTags))
		dataSource?.clearSelectedTags()
	}
	
	@objc func popController() {
		do {
			try CoreStack.mainContext.save()
		} catch {
			debugPrint(error)
		}
		navigationController?.popViewController(animated: true)
	}
}

private extension TagSearchViewController {
	func configureNavigationBar() {
		let clearButton = UIButton(type: .custom)
		clearButton.setTitle(clearButtonText, for: .normal)
		clearButton.setTitleColor(Colors.red, for: .normal)
		clearButton.addTarget(self, action: #selector(removeTags), for: .touchUpInside)
		let rightItem = UIBarButtonItem(customView: clearButton)
		navigationItem.setRightBarButton(rightItem, animated: true)
		navigationItem.title = sectionTitle
		if #available(iOS 11.0, *) {
			navigationItem.largeTitleDisplayMode = .always
		}
	}
	
	func configureCollectionView() {
		let dataSourceType = TagsDataSourceType.multipleSection(sectionKey: "type")
		dataSource = TagSearchDataSource(container: self, type: dataSourceType, tagsCount: 100)
		let layout = TagSearchCollectionViewLayout(type: dataSourceType)
		layout.delegate = dataSource
		collectionView.collectionViewLayout = layout
		collectionView.delegate = dataSource
		collectionView.dataSource = dataSource
		collectionView.register(R.nib.tagCollectionViewCell)
		collectionView.register(R.nib.tagSearchHeaderView, forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader")
	}
}

extension TagSearchViewController: UINavigationControllerDelegate {
	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		if toVC.isKind(of: StickersFeedViewController.self) && operation == .pop {
			return TagSearchDismissionAnimator()
		}
		return nil
	}
}

extension TagSearchViewController {
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.delegate = self
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Analys.log(.open(screen: .allTagsList))
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		coordinator.animate(alongsideTransition: { [unowned self] context in
			self.collectionView?.collectionViewLayout.invalidateLayout()
			}, completion: nil)
	}
}

extension TagSearchViewController: Storyboardable {
	static func storyBoardName() -> String {
		return "StickersFinder"
	}
}
