//
//  StickerPacksViewController.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import ObjectMapper
import AXPhotoViewer

class StickersFeedViewController: UIViewController {
	
	var dataSource: StickersFeedCollectionViewDataSource?
	var dataLoader: StickersDataLoader?
	
	private var delegate: StickersFeedCollectionViewDelegate?
	private var searchController: UISearchController!
	private var lastQuery: String?
	
	private var stickersSearchController: StickersSearchViewController?
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		configureDataSource()
		configureDelegate()
		configureCollectionView()
		configureStickersSearchController()
		configureNavigationBar()
		configureForceTouch()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.delegate = self
		if let cell = collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? ContainerCollectionViewCell {
			cell.reloadData()
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Analys.log(.open(screen: .stickersFeed))
	}
	
	@objc func openTagsSearch() {
		Analys.log(.tapButton(button: .showMoreTags))
		do {
			try CoreStack.mainContext.save()
		} catch {
			debugPrint(error)
		}
		navigationController?.pushViewController(TagSearchViewController.create(), animated: true)
	}
	
	@objc func openSearch() {
		Analys.log(.tapButton(button: .searchStickers))
		guard let searchScreen = stickersSearchController else { return }
		navigationController?.pushViewController(searchScreen, animated: true)
	}
	
	@objc func addStickerSet() {
		Analys.log(.tapButton(button: .proposeSticker))
	}
}

extension StickersFeedViewController: UINavigationControllerDelegate {
	func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		guard toVC.isKind(of: TagSearchViewController.self) && operation == .push else { return nil }
		return TagSearchPresentationAnimator()
	}
}

private extension StickersFeedViewController {
	func configureDelegate() {
		delegate = StickersFeedCollectionViewDelegate(controller: self)
	}
	
	func configureDataSource() {
		dataSource = StickersFeedCollectionViewDataSource(viewController: self)
	}
	
	func configureCollectionView() {
		collectionView.register(R.nib.stickerPackCollectionViewCell)
		collectionView.register(R.nib.containerCollectionViewCell)
		collectionView.register(R.nib.showMoreCollectionViewCell)
		collectionView.register(R.nib.tagsSearchHeaderView,
								forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader")
		collectionView.register(R.nib.stickerSetsHeaderView,
								forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader")
		collectionView.delegate = delegate
		collectionView.dataSource = dataSource
	}
	
	func configureStickersSearchController() {
		stickersSearchController = StickersSearchViewController.create()
	}
	
	func configureNavigationBar() {
//		let rightItem = UIBarButtonItem(image: R.image.navbar_add()!, style: .plain, target: self, action: #selector(addStickerSet))
		let leftItem = UIBarButtonItem(image: R.image.navbar_search()!, style: .plain, target: self, action: #selector(openSearch))
//		navigationItem.setRightBarButton(rightItem, animated: true)
		navigationItem.setLeftBarButton(leftItem, animated: true)
		
		if #available(iOS 11.0, *) {
			navigationController?.navigationBar.prefersLargeTitles = true
			navigationItem.largeTitleDisplayMode = .automatic
		}
	}
	
	func configureForceTouch() {
		registerForPreviewing(with: self, sourceView: collectionView)
	}
}

extension StickersFeedViewController: UIViewControllerPreviewingDelegate {
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
		if let previewingPhotosViewController = viewControllerToCommit as? AXPreviewingPhotosViewController {
			self.present(AXPhotosViewController(from: previewingPhotosViewController), animated: false)
		}
	}
	
	func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
		guard let indexPath = collectionView.indexPathForItem(at: location),
			let cell = collectionView.cellForItem(at: indexPath),
			let stickerSet = dataSource?[indexPath] else { return nil }
		
		previewingContext.sourceRect = collectionView.convert(cell.contentView.frame, from: cell.contentView.superview)
		let image = stickerSet.photosDataSourceThumb(item: 0)
		let data = AXPhotosDataSource(photos: [image])
		let previewController = AXPhotosViewController(dataSource: data)
		previewController.overlayView.toolbar.removeFromSuperview()
		previewController.view.backgroundColor = .clear
		Analys.log(.preview(item: .stickerSet, itemId: stickerSet.name))
		return previewController
	}
}

extension StickersFeedViewController: Storyboardable {
	static func storyBoardName() -> String {
		return "StickersFinder"
	}
}
