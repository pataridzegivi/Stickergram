//
//  StickersFeedCollectionViewDelegate.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 05.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class StickersFeedCollectionViewDelegate: NSObject {
	
	let stickerSize = CGSize(width: 150, height: 168)
	let headerHeight: CGFloat = 40
	let tagsCellHeight: CGFloat = ContainerCollectionViewCell.cellHeight(rows: 5)
	let showMoreCellHeight: CGFloat = 36
	
	weak var viewController: StickersFeedViewController?
	
	convenience init(controller: StickersFeedViewController) {
		self.init()
		self.viewController = controller
	}
}

extension StickersFeedCollectionViewDelegate: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if indexPath.section == 0 && Config.isTagsEnabled {
			viewController?.openTagsSearch()
			return
		}
		guard let set = viewController?.dataSource?[indexPath] else { return }
		Analys.log(.tapItem(item: .stickerSet, itemId: set.name))
		let stickersViewController = StickersViewController.create(with: set)
		viewController?.navigationController?.pushViewController(stickersViewController, animated: true)
	}
	
	func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if let cell = cell as? StickerPackCollectionViewCell {
			cell.didEndDisplaying()
		}
		if let cell = cell as? ContainerCollectionViewCell {
			cell.reloadData()
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		guard let dataSource = viewController?.dataSource else { return }
		guard indexPath.section != 0 || !Config.isTagsEnabled else { return }
		let totalCells = collectionView.numberOfItems(inSection: indexPath.section)
		if indexPath.item > totalCells - 10 {
			Analys.log(.scroll(list: .popular, page: UInt(totalCells) / dataSource.itemsPerPage))
			dataSource.nextPage()
		}
	}
}

extension StickersFeedCollectionViewDelegate: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		if indexPath.section == 0 && Config.isTagsEnabled {
			let height = (indexPath.item == 0 ? tagsCellHeight : showMoreCellHeight)
			return CGSize(width: collectionView.bounds.size.width, height: height)
		} else {
			return stickerSize
		}
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		if section == 0 && Config.isTagsEnabled {
			return UIEdgeInsets.zero
		}
		let itemsPerRow = floor(collectionView.bounds.size.width / stickerSize.width)
		let excessLength = collectionView.bounds.size.width - (stickerSize.width * itemsPerRow)
		let inset = excessLength / (itemsPerRow + 1)
		return UIEdgeInsetsMake(0, inset, 0, inset)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		guard section != 0 else { return 0 }
		return 8
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return CGSize(width: collectionView.bounds.size.width, height: headerHeight)
	}
}
