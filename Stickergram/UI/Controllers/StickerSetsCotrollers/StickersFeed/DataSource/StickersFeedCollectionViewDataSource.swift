//
//  StickerSetsDataSource.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 08.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import CoreData

enum TagsDataSourceType {
	case oneSection
	case multipleSection(sectionKey: String)
}

enum StickersAddingType {
	case insertToEnd
	case reloadAll
}

class StickersFeedCollectionViewDataSource: NSObject {
	
	private var stickerSets: [StickerSet] = []
	private var dataLoader: StickersDataLoader?
	private var viewController: StickersFeedViewController
	private let stickersSection = Config.isTagsEnabled ? 1 : 0
	
	private var fetchedResultsController: NSFetchedResultsController<Tag>?
	private var tagsFilter: Set<Tag> = [] {
		didSet {
			if !tagsFilter.isEmpty {
				filterByTags()
			} else {
				firstPage()
			}
		}
	}
	
	let itemsPerPage: UInt = 20
	
	var stickersCount: Int {
		return stickerSets.count
	}
	
	init(viewController: StickersFeedViewController) {
		self.viewController = viewController
		super.init()
		self.dataLoader = StickersDataLoader(delegate: self, pageCount: itemsPerPage)
		configureFetchedResultsController()
		firstPage()
	}
	
	func firstPage() {
		dataLoader?.getStickersByRating()
	}
	
	func filterByTags() {
		dataLoader?.getStickersBy(tags: tagsFilter.map({ $0.name }))
	}
	
	func nextPage() {
		dataLoader?.next()
	}
	
	func configureFetchedResultsController() {
		let request = NSFetchRequest<Tag>(entityName: Tag.className())
		request.predicate = NSPredicate(format: "selected == true")
		let nameSelected = NSSortDescriptor(key: "name", ascending: true)
		request.sortDescriptors = [nameSelected]
		fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreStack.mainContext, sectionNameKeyPath: nil, cacheName: nil)
		fetchedResultsController?.delegate = self
		try? fetchedResultsController?.performFetch()
	}
	
	subscript(index: IndexPath) -> StickerSet? {
		guard index.item < stickerSets.count else { return nil }
		return stickerSets[index.item]
	}
}

extension StickersFeedCollectionViewDataSource: NSFetchedResultsControllerDelegate {
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		guard let object = anObject as? Tag else { return }
		if object.selected {
			tagsFilter.insert(object)
		} else {
			tagsFilter.remove(object)
		}
	}
}

extension StickersFeedCollectionViewDataSource: StickersDataLoaderDelegate {
	func add(stickers: [StickerSet], firstPage: Bool) {
		guard !stickers.isEmpty else { return }
		if firstPage {
			stickerSets = stickers
			viewController.collectionView.reloadSections(IndexSet(integer: stickersSection))
		} else {
			let indexes = Array<Int>(stickerSets.count...stickerSets.count + stickers.count - 1)
			let indexPathes = IndexPath.indexes(indexes, section: stickersSection)
			stickerSets.append(contentsOf: stickers)
			viewController.collectionView.insertItems(at: indexPathes)
		}
	}
}

extension StickersFeedCollectionViewDataSource {
	///Tag Search Section configuration
	func tagSearchSection(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if indexPath.item == 0 {
			return collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.containerCollectionViewCell, for: indexPath)!
		} else {
			let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.showMoreCollectionViewCell, for: indexPath)!
			return cell
		}
	}
	
	func tagSearchHeader(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: R.reuseIdentifier.tagsSearchHeaderView, for: indexPath)!
		return header
	}
	
	///Tag Search Section configuration
	func stickersSection(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.stickerPackCollectionViewCell,
													  for: indexPath)!
		cell.stickerSet = stickerSets[indexPath.item]
		return cell
	}
	
	func stickersHeader(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: R.reuseIdentifier.stickerSetsHeaderView, for: indexPath)!
	}
}

extension StickersFeedCollectionViewDataSource: UICollectionViewDataSourcePrefetching {
	public func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
		
	}
}

extension StickersFeedCollectionViewDataSource: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if indexPath.section == 0 && Config.isTagsEnabled  {
			return tagSearchSection(collectionView, cellForItemAt: indexPath)
		} else {
			return stickersSection(collectionView, cellForItemAt: indexPath)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if section == 0 && Config.isTagsEnabled {
			return 2
		} else {
			return stickerSets.count
		}
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		if Config.isTagsEnabled {
			return 2
		} else {
			return 1
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		if indexPath.section == 0 && Config.isTagsEnabled {
			return tagSearchHeader(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
		} else {
			return stickersHeader(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
		}
	}
}
