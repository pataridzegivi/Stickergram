//
//  StickersSearchCollectionViewDelegate.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 26.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class StickersSearchCollectionViewDelegate: NSObject, StickerSearchDelegateProtocol {
	
	let stickerSize = CGSize(width: 150, height: 168)
	let headerHeight: CGFloat = 56
	
	weak var controller: StickerSearchControllerProtocol?
	
	convenience init(controller: StickerSearchControllerProtocol) {
		self.init()
		self.controller = controller
	}
}

extension StickersSearchCollectionViewDelegate: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		controller?.didSelectItem(at: indexPath)
	}
	
	func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		if let cell = cell as? StickerPackCollectionViewCell {
			cell.didEndDisplaying()
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		let totalCells = collectionView.numberOfItems(inSection: indexPath.section)
		if indexPath.item > totalCells - 10 {
			controller?.loadNextPage()
		}
	}
}

extension StickersSearchCollectionViewDelegate: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return stickerSize
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		let itemsPerRow = floor(collectionView.bounds.size.width / stickerSize.width)
		let excessLength = collectionView.bounds.size.width - (stickerSize.width * itemsPerRow)
		let inset = excessLength / (itemsPerRow + 1)
		return UIEdgeInsetsMake(0, inset, 0, inset)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 8
	}
}
