//
//  StickerSearchDelegateProtocol.swift
//  Stickergram
//
//  Created by Givi Pataridze on 05.01.2018.
//  Copyright © 2018 Givi Pataridze. All rights reserved.
//

import UIKit

protocol StickerSearchDelegateProtocol: class, UICollectionViewDelegateFlowLayout {
	weak var controller: StickerSearchControllerProtocol? { get set }
}
