//
//  StickerSearchControllerProtocol.swift
//  Stickergram
//
//  Created by Givi Pataridze on 05.01.2018.
//  Copyright © 2018 Givi Pataridze. All rights reserved.
//

import UIKit

protocol StickerSearchControllerProtocol: class {
	func didSelectItem(at indexPath: IndexPath)
	func loadNextPage()
	func refresh()
	func add(at indexPathes:[IndexPath])
}
