//
//  StickersSearchDataSourceProtocol.swift
//  Stickergram
//
//  Created by Givi Pataridze on 05.01.2018.
//  Copyright © 2018 Givi Pataridze. All rights reserved.
//

import UIKit

protocol StickersSearchDataSourceProtocol: class, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching {
	
	var searchHistory: [String] { get set }
	var itemsPerPage: UInt { get }
	var count: Int { get }
	
	weak var controller: StickerSearchControllerProtocol? { get set }
	
	func startSearch(query: String)
	func nextPage()
	
	subscript(index: IndexPath) -> StickerSetProtocol? { get }
}
