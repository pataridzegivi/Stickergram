//
//  StickersSearchCollectionViewDataSource.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 26.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class StickersSearchCollectionViewDataSource: NSObject, StickersSearchDataSourceProtocol {

	private var stickerSets: [StickerSet] = []
	private var dataLoader: StickersDataLoader?
	
	weak var controller: StickerSearchControllerProtocol?
	
	var itemsPerPage: UInt {
		return 10
	}
	
	var count: Int {
		return stickerSets.count
	}
	
	var searchHistory: [String] = []
	
	init(viewController: StickerSearchControllerProtocol) {
		self.controller = viewController
		super.init()
		self.dataLoader = StickersDataLoader(delegate: self, pageCount: itemsPerPage)
	}

	subscript(index: IndexPath) -> StickerSetProtocol? {
		guard index.item < stickerSets.count else { return nil }
		return stickerSets[index.item]
	}
	
	func startSearch(query: String) {
		guard !query.isEmpty else { return }
		searchHistory.append(query)
		dataLoader?.getStickersBy(query: query)
	}
	
	func nextPage() {
		dataLoader?.next()
	}
}

extension StickersSearchCollectionViewDataSource: StickersDataLoaderDelegate {
	func add(stickers: [StickerSet], firstPage: Bool) {
		if firstPage {
			stickerSets = stickers
			controller?.refresh()
		} else {
			let indexes = Array<Int>(stickerSets.count...stickerSets.count + stickers.count - 1)
			let indexPathes = IndexPath.indexes(indexes, section: 0)
			stickerSets.append(contentsOf: stickers)
			controller?.add(at: indexPathes)
		}
	}
}

extension StickersSearchCollectionViewDataSource {
	public func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
	}
}

extension StickersSearchCollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.stickerPackCollectionViewCell,
													  for: indexPath)!
		cell.stickerSet = stickerSets[indexPath.item]
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return stickerSets.count
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
}
