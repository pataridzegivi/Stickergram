//
//  StickersSearchViewController.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 26.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import CoreData

class StickersSearchViewController: UIViewController {

	let StringSearchScreenTitle = NSLocalizedString("Stickers search", comment: "Search bar placeholder string")
	let StringSearchPlaceholderText = NSLocalizedString("Find sticker", comment: "Search bar placeholder string")
	let StringEmptySearchFirstTimePlaceholderText = NSLocalizedString("😳\n You haven't searched for anything yet", comment: "Empty search screen placeholder when first search")
	let StringEmptySearchResultsPlaceholderText = NSLocalizedString("😢\n Sorry, but nothing was found... try another request", comment: "Empty search screen placeholder when search results is empty")
	
	var dataSource: StickersSearchDataSourceProtocol!
	var delegate: StickerSearchDelegateProtocol!
	
	private var lastQuery: String?
	private var searchController: UISearchController!
	
	private var timer: Timer? {
		didSet {
			oldValue?.invalidate()
		}
	}
	private var lastQueryDate: Date?
	private var queryDelay: TimeInterval = 0.5
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var emptySearchLabel: UILabel!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		configureEmptyScreen()
		configureDependencies()
		configureNavigationBar()
		configureCollectionView()
		configureSearchController()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Analys.log(.open(screen: .stickersFinder))
		if lastQuery == nil {
			searchController.isActive = true
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if let lastQuery = lastQuery {
			searchController.searchBar.text = lastQuery
		}
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		searchController.isActive = false
		timer?.invalidate()
		timer = nil
	}
	
	@objc func performSearch() {
		guard let query = lastQuery else { return }
		Analys.log(.search(item: .stickerSet, query: query))
		dataSource.startSearch(query: query)
	}
}

extension StickersSearchViewController: StickerSearchControllerProtocol {
	func didSelectItem(at indexPath: IndexPath) {
		guard let set = dataSource[indexPath] as? StickerSet else { return }
		Analys.log(.tapItem(item: .stickerSet, itemId: set.name))
		let stickersViewController = StickersViewController.create(with: set)
		navigationController?.pushViewController(stickersViewController, animated: true)
	}
	
	func loadNextPage() {
		dataSource.nextPage()
	}
	
	func refresh() {
		if dataSource.searchHistory.isEmpty {
			emptySearchLabel.text = StringEmptySearchFirstTimePlaceholderText
			emptySearchLabel.isHidden = false
		} else {
			if dataSource.count > 0 {
				emptySearchLabel.isHidden = true
			} else {
				emptySearchLabel.text = StringEmptySearchResultsPlaceholderText
				emptySearchLabel.isHidden = false
			}
		}
		
		collectionView.reloadSections(IndexSet(integer: 0))
	}
	
	func add(at indexPathes: [IndexPath]) {
		collectionView.insertItems(at: indexPathes)
	}
}

extension StickersSearchViewController: UISearchBarDelegate {
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
	}
	
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		lastQuery = searchText
		timer = Timer.scheduledTimer(timeInterval: queryDelay, target: self, selector: #selector(performSearch), userInfo: nil, repeats: false)
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
	}
}

extension StickersSearchViewController: UISearchControllerDelegate {
	func didPresentSearchController(_ searchController: UISearchController) {
	}
	
	func willPresentSearchController(_ searchController: UISearchController) {
		if #available(iOS 11.0, *) {
			navigationItem.hidesSearchBarWhenScrolling = false
		}
	}
	
	func didDismissSearchController(_ searchController: UISearchController) {
	}
	
	func willDismissSearchController(_ searchController: UISearchController) {
		if #available(iOS 11.0, *) {
			navigationItem.hidesSearchBarWhenScrolling = true
		}
	}
	
	func presentSearchController(_ searchController: UISearchController) {
		searchController.searchBar.becomeFirstResponder()
	}
}

extension StickersSearchViewController: UISearchResultsUpdating {
	func updateSearchResults(for searchController: UISearchController) {

	}
}

extension StickersSearchViewController {
	func configureEmptyScreen() {
		emptySearchLabel.text = StringEmptySearchFirstTimePlaceholderText
	//	emptySearchLabel.isHidden = true
	}
	
	func configureDependencies() {
		delegate = StickersSearchCollectionViewDelegate(controller: self)
		dataSource = StickersSearchCollectionViewDataSource(viewController: self)
	}
	
	func configureNavigationBar() {
  		navigationController?.interactivePopGestureRecognizer?.delegate = nil
		title = StringSearchScreenTitle
	}
	
	func configureCollectionView() {
		collectionView.register(R.nib.emptySearchResultsCollectionViewCell)
		collectionView.register(R.nib.stickerPackCollectionViewCell)
		collectionView.delegate = delegate
		collectionView.dataSource = dataSource
		collectionView.keyboardDismissMode = .onDrag
		(collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionHeadersPinToVisibleBounds = true
	}
	
	func configureSearchController() {
		searchController = UISearchController(searchResultsController: nil)
		searchController.delegate = self
		searchController.searchResultsUpdater = self
		searchController.searchBar.showsCancelButton = false
		searchController.dimsBackgroundDuringPresentation = false
		
		if #available(iOS 11.0, *) {
			navigationItem.searchController = searchController
			navigationItem.hidesSearchBarWhenScrolling = false
		} else {
			searchController.hidesNavigationBarDuringPresentation = false
			navigationItem.leftBarButtonItem = nil
			navigationItem.rightBarButtonItem = nil
			navigationItem.titleView = searchController.searchBar
		}
		
		definesPresentationContext = true
		searchController.searchBar.delegate = self
	}
}

extension StickersSearchViewController: Storyboardable {
	static func storyBoardName() -> String {
		return "StickersFinder"
	}
}
