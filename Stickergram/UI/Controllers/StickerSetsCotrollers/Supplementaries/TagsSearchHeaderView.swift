//
//  TagsSearchHeaderView.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import CoreData

class TagsSearchHeaderView: UICollectionReusableView {
	
	let titleDefaultText = NSLocalizedString("Tags", comment: "String that appears in header of tags cell")
	
	private var fetchedResultsController: NSFetchedResultsController<Tag>?
	
	@IBOutlet private weak var removeTagsButton: UIButton!
	@IBOutlet weak var titleLabel: UILabel!
	
	@IBAction private func removeTagsButtonClicked(_ sender: Any) {
		Analys.log(.tapButton(button: .clearAllTags))
		NotificationCenter.default.post(Notification.clearSelectedTags)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		titleLabel.text = titleDefaultText
		
		let request = NSFetchRequest<Tag>(entityName: Tag.className())
		request.predicate = NSPredicate(format: "selected == true")
		let nameSelected = NSSortDescriptor(key: "name", ascending: true)
		request.sortDescriptors = [nameSelected]
		fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreStack.mainContext, sectionNameKeyPath: nil, cacheName: nil)
		fetchedResultsController?.delegate = self
		try? fetchedResultsController?.performFetch()
		if let result = try? CoreStack.mainContext.fetch(request) {
			removeTagsButton.isHidden = result.isEmpty
		}
	}
}

extension TagsSearchHeaderView: NSFetchedResultsControllerDelegate {
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		guard let objects = controller.fetchedObjects else { return }
		removeTagsButton.isHidden = objects.isEmpty
	}
}
