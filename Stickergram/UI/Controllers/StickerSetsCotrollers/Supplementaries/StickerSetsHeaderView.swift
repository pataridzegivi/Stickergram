//
//  StickerSetsHeaderView.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class StickerSetsHeaderView: UICollectionReusableView {

	let titleDefaultText = NSLocalizedString("Stickers feed", comment: "String that appears in header of stickers section")

	@IBOutlet private weak var titleLabel: UILabel!

	override func awakeFromNib() {
		super.awakeFromNib()
		titleLabel.text = titleDefaultText
	}
}
