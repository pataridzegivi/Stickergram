//
//  ContainerCollectionViewCell.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class ContainerCollectionViewCell: UICollectionViewCell, TagsDataSourceDelegate {
	
	private var rowsCount: Int = 5
	private var dataSource: TagSearchDataSource?
	private let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
	
	private var hasData: Bool = false
	
	@IBOutlet weak var collectionView: UICollectionView!
	
	override func awakeFromNib() {
        super.awakeFromNib()
		configureCollectionView()
		configureActivityIndicator()
    }
	
	override func layoutSubviews() {
		super.layoutSubviews()
		indicator.center = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
	}
	
	func reloadData() {
		dataSource?.loadData()
	}
	
	func didLoadingCompleted() {
		hasData = true
		indicator.stopAnimating()
		collectionView.reloadData()
	}
}

extension ContainerCollectionViewCell {
	static func cellHeight(rows: Int) -> CGFloat {
		return TagSearchCollectionViewLayout.itemHeight * CGFloat(rows)
	}
}

extension ContainerCollectionViewCell {
	func configureActivityIndicator() {
		indicator.hidesWhenStopped = true
		collectionView.addSubview(indicator)
		if !hasData {
			indicator.startAnimating()
		}
	}
	
	func configureCollectionView() {
		let dataSourceType = TagsDataSourceType.oneSection
		dataSource = TagSearchDataSource(container: self, type: dataSourceType, tagsCount: rowsCount * 5)
		let layout = TagSearchCollectionViewLayout(type: dataSourceType)
		layout.delegate = dataSource
		collectionView.collectionViewLayout = layout
		collectionView.delegate = dataSource
		collectionView.dataSource = dataSource
		collectionView.register(R.nib.tagCollectionViewCell)
		collectionView.register(R.nib.tagSearchHeaderView, forSupplementaryViewOfKind: "UICollectionElementKindSectionHeader")
	}
}
