//
//  StickerPackCollectionViewCell.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 14.10.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import Kingfisher
import KingfisherWebP


class StickerPackCollectionViewCell: UICollectionViewCell {
	
	var stickerSet: StickerSet? {
		didSet {
			guard let set = stickerSet else { return }
			title.text = set.title
			image.kf.indicatorType = .activity
			image.kf.setWebPImage(with: set.thumbUrl())
		}
	}
	
	@IBOutlet private weak var image: UIImageView!
	@IBOutlet private weak var title: UILabel!

	func didEndDisplaying() {
		image.kf.cancelDownloadTask()
	}
}
