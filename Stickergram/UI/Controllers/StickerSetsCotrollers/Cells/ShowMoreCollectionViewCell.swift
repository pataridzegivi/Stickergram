//
//  ShowMoreCollectionViewCell.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 17.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class ShowMoreCollectionViewCell: UICollectionViewCell {

	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var showMoreLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 6
    }

}
