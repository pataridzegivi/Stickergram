//
//  ProgressHUD.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 25.12.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class ProgressHUD {
	static let shared = ProgressHUD()
	fileprivate var alert: UIAlertController!
	private init() {}
}

extension UIViewController {
	func showHUD(text: String) {
		ProgressHUD.shared.alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
		let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
		loadingIndicator.hidesWhenStopped = true
		loadingIndicator.activityIndicatorViewStyle = .gray
		loadingIndicator.startAnimating()
		ProgressHUD.shared.alert.view.addSubview(loadingIndicator)
		present(ProgressHUD.shared.alert, animated: true, completion: nil)
	}
	
	func hideHUD() {
		ProgressHUD.shared.alert.dismiss(animated: true, completion: nil)
	}
	
	func showError(text: String, completion: (()->())?) {
		let alert = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
		let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
		alert.addAction(ok)
		present(alert, animated: true) {
			completion?()
		}
	}
}
