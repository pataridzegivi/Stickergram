//
//  TabBarViewController.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 03.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import FirebaseAuth

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		configureChildControllers()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
	}

}


extension TabBarViewController {
	private func configureChildControllers() {
		let stickersController = UINavigationController(rootViewController: StickersFeedViewController.create())
		stickersController.navigationBar.topItem?.title = NSLocalizedString("Stickergram", comment: "NavBar Search icon title")
		if #available(iOS 10.0, *) {
			stickersController.tabBarItem.badgeColor = Colors.base
		} else {
			tabBar.tintColor = Colors.base
		}
		stickersController.tabBarItem.image = R.image.tabbar_search()
		stickersController.tabBarItem.title = NSLocalizedString("Sticker finder", comment: "TabBar search icon title")
		
		let favoritesController = UINavigationController(rootViewController: TagSearchViewController.create())
		favoritesController.navigationBar.topItem?.title = NSLocalizedString("My favorites", comment: "NavBar Favorites icon title")
		if #available(iOS 10.0, *) {
			favoritesController.tabBarItem.badgeColor = Colors.base
		} else {
			tabBar.tintColor = Colors.base
		}
		favoritesController.tabBarItem.image = R.image.tabbar_favorite()
		favoritesController.tabBarItem.title = NSLocalizedString("My favorites", comment: "TabBar Favorites icon title")
		favoritesController.title = NSLocalizedString("My favorites", comment: "NavBar favorites icon title")
		
		setViewControllers([stickersController, favoritesController], animated: true)
	}
}
