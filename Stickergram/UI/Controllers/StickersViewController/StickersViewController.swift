//
//  StickersViewController.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 03.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit
import AXPhotoViewer

class StickersViewController: UIViewController {

	private let stickerSize = CGSize(width: 120, height: 120)
	
	var stickerSet: StickerSetProtocol!
	var photosViewController: AXPhotosViewController?
	var photosDataSource: AXPhotosDataSource?
	
	@IBOutlet private weak var collectionView: UICollectionView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		configureViewController()
		configureCollectionView()
		configureNavigationBar()
    }
 
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		UIView.appearance().tintColor = Colors.base	
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Analys.log(.open(screen: .stickerSetDetail))
	}
	
	@objc func addToTelegram() {
		Analys.log(.tapButton(button: .addSticker))
		TelegramRouter.addSet(set: stickerSet)
	}
}

extension StickersViewController: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let dataSource = AXPhotosDataSource(photos: stickerSet.photosDataSource(), initialPhotoIndex: indexPath.item)
		let controller = AXPhotosViewController(dataSource: dataSource)
		UIView.appearance().tintColor = .white
		controller.overlayView.rightBarButtonItem = UIBarButtonItem()
		present(controller, animated: true, completion: {
			Analys.log(.open(screen: .stickerSetPager))
		})
	}
}

extension StickersViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.stickerCollectionViewCell, for: indexPath)!
		cell.stickerUrl = stickerSet.stickerThumbUrl(number: indexPath.item)
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return stickerSet.count
	}
}

extension StickersViewController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return stickerSize
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		return 8
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		let itemsPerRow = floor(collectionView.bounds.size.width / stickerSize.width)
		let excessLength = collectionView.bounds.size.width - (stickerSize.width * itemsPerRow)
		let inset = excessLength / (itemsPerRow + 1)
		return UIEdgeInsetsMake(0, inset, 0, inset)
	}
}

extension StickersViewController {
	private func configureViewController() {
		title = stickerSet.title
	}
	
	private func configureCollectionView () {
		collectionView.register(R.nib.stickerCollectionViewCell)
	}
	
	private func configureNavigationBar() {
		guard let icon = R.image.navbar_plus() else { return }
		let rightItem = UIBarButtonItem(image: icon,
										style: .plain,
										target: self,
										action: #selector(addToTelegram))
		navigationItem.rightBarButtonItem = rightItem
	}
}

extension StickersViewController: Storyboardable {
	static func storyBoardName() -> String {
		return "StickersFinder"
	}
	
	static func create(with stickerSet: StickerSetProtocol) -> StickersViewController {
		let controller = StickersViewController.create()
		controller.stickerSet = stickerSet
		return controller
	}
}
