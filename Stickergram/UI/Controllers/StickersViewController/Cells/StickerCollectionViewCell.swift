//
//  StickerCollectionViewCell.swift
//  TeleStickers
//
//  Created by Givi Pataridze on 05.11.2017.
//  Copyright © 2017 Givi Pataridze. All rights reserved.
//

import UIKit

class StickerCollectionViewCell: UICollectionViewCell {

	var stickerUrl: URL? {
		didSet {
			image.kf.indicatorType = .activity
			image.kf.setWebPImage(with: stickerUrl)
		}
	}
	
	@IBOutlet private weak var image: UIImageView!
	
}
